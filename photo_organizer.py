#!/usr/bin/env python
from __future__ import print_function

__author__ = "Stephen Lin"
__copyright__ = "Copyright (C) 2017 Stephen Lin"
__license__ = "Public Domain"
__version__ = "0.2"
__doc__ = """Utility to organize photos by date."""


import argparse
import os
import platform
import shutil
import sys
import time
from PIL import Image
from datetime import datetime

ON_WIN = platform.system() == "Windows"
IMAGE_FILE_EXTS = [".JPG", ".CR2", ".CR3", ".MOV", ".PNG"]

def achieve(input_dir, output_dir, ignores):
  for root, dirs, files in os.walk(input_dir, topdown=False):
    for f in files:
      _, ext = os.path.splitext(f)
      if ext.upper() in IMAGE_FILE_EXTS:
        path = os.path.join(root, f)
        skip = False
        for ignore in ignores:
          if ignore in path:
            skip = True
        if not skip:
          t = 0
          try:
            exif_date_string = exif_creation_date(path)
            dt = datetime.strptime(exif_date_string, '%Y:%m:%d %H:%M:%S')
            t = dt.timetuple()
          except :
            skip

          if not t:
            file_create_time = file_creation_date(path)
            t = time.localtime(file_create_time)
          target_dir = "{0.tm_year}/{0.tm_mon:02d}/{0.tm_year}-{0.tm_mon:02d}-{0.tm_mday:02d}".format(t)
          target_dir = os.path.join(output_dir, target_dir)
          if path != os.path.join(target_dir, f):
            print("{} ==> {}".format(path, target_dir))
            move_file(path, target_dir)


def exif_creation_date(path_to_file):
  """
  Try to get the date that a file was created, falling back to when it was
  last modified if that isn't possible.
  See http://stackoverflow.com/a/39501288/1709587 for explanation.
  """
  return Image.open(path_to_file)._getexif()[36867]


def file_creation_date(path_to_file):
  """
  Try to get the date that a file was created, falling back to when it was
  last modified if that isn't possible.
  See http://stackoverflow.com/a/39501288/1709587 for explanation.
  """
  if ON_WIN:
    return os.path.getctime(path_to_file)
  else:
    stat = os.stat(path_to_file)
    try:
      return stat.st_birthtime
    except AttributeError:
      # We're probably on Linux. No easy way to get creation dates here,
      # so we'll settle for when its content was last modified.
      return stat.st_mtime


def move_file(f, target_dir):
  target_path = os.path.join(target_dir)
  if not os.path.exists(target_path):
    os.makedirs(target_path)

  if not os.path.isdir(target_path):
    print("Target directory {} is not a directory".format(target_dir))
    return

  if not os.path.exists(os.path.join(target_path, os.path.basename(f))):
    shutil.move(f, target_path)


def clean_empty_dir(path, ignores):
  for root, dirs, files in os.walk(path, topdown=False):
    if len(dirs) == 0 and len(files) == 0:
      for ignore in ignores:
        if ignore in root:
          continue
      print("Delete {}\n".format(root))
      os.rmdir(root)


def main(arguments):
  parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)
  parser.add_argument('input_dir', help="Input directory")
  parser.add_argument('-od', '--output_dir', help="Output directory")
  parser.add_argument('-i', '--ignore', action='append', help="ignore pattern")

  args = parser.parse_args(arguments)
  input_root = os.path.join(args.input_dir)

  output_dir = args.output_dir
  if not output_dir:
    output_dir = input_root
    print("Output directory not specified, output to input directory {}.\n".format(output_dir))

  if not os.path.exists(input_root):
    print("Input directory {} doesn't exist.\n".format(input_root))

  # Walk through all files in input directory
  print("Organize photo {} ==> {}.\n".format(input_root, output_dir))
  achieve(input_root, output_dir, args.ignore)

  clean_empty_dir(output_dir, args.ignore)


if __name__ == '__main__':
  sys.exit(main(sys.argv[1:]))